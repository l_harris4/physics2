// This file is used to laod the models
#include "cGameObject.h"
#include <vector>
#include "Utilities.h"		// getRandInRange()
#include <glm/glm.hpp>
#include "globalGameStuff.h"
#include "globalOpenGL_GLFW.h"
#include "cLightManager.h"
#include <sRigidBodyDesc.h>
#include <iPhysicsFactory.h>
#include <iShape.h>
//#include <Physics\MySuperPhysics\cPhysicsFactory.h>

extern nPhysics::iPhysicsFactory* gPhysicsFactory;
extern nPhysics::iPhysicsWorld* gPhysicsWorld;


extern std::vector< cGameObject* >  g_vecGameObjects;
//extern cGameObject* g_pTheDebugSphere;
//extern cBasicTextureManager*	g_pTextureManager = 0;


//const float SURFACEOFGROUND = -10.0f;
//const float RIGHTSIDEWALL = 15.0f;
//const float LEFTSIDEWALL = -15.0f;

void LoadModelsIntoScene(void)
{
	return;
}

bool LoadModelsLightsFromFile()
{
	std::string filename = "ObjectsConfig.txt";
	std::string line;
	std::ifstream inFile(filename.c_str());
	int lightIndex = -1;

	glm::vec3 normal = glm::vec3(0, 1, 0);


	try {
		while (getline(inFile, line))
		{
			if (line == "")
			{
				continue;
			}
			if (line == "Object Start") {
				cGameObject* pTempGO = new cGameObject();
				pTempGO->bIsUpdatedInPhysics = false;
				pTempGO->typeOfObject = eTypeOfObject::SPHERE;
				pTempGO->radius = 1.0f;
				sMeshDrawInfo meshInfo;
				pTempGO->vecMeshes.push_back(meshInfo);
				meshInfo.debugDiffuseColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
				pTempGO->diffuseColour = glm::vec4(0, 1, 1, 1);

				::g_vecGameObjects.push_back(pTempGO);
				continue;
			}

			if (line.find("type:") != std::string::npos)
			{
				if (line.find("sphere") != std::string::npos)
				{
					::g_vecGameObjects.back()->typeOfObject = eTypeOfObject::SPHERE;
				}
				else if (line.find("plane") != std::string::npos)
				{
					::g_vecGameObjects.back()->typeOfObject = eTypeOfObject::PLANE;
				}
			}
			if (line.find("texture1:") != std::string::npos || line.find("texture2:") != std::string::npos) {
				line.replace(0, 10, "");
				if (::g_vecGameObjects.back()->isSkybox)
				{
					::g_vecGameObjects.back()->vecMeshes[0].vecMeshCubeMaps.push_back(sTextureBindBlendInfo("space", 1.0f));
				}
				else
				{
					::g_vecGameObjects.back()->vecMeshes[0].vecMehs2DTextures.push_back(sTextureBindBlendInfo(line, 1.0f));
					::g_pTextureManager->Create2DTextureFromBMPFile(line, true);
				}
				
				continue;
			}
			if (line.find("texture2:") != std::string::npos) {
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->vecMeshes[0].vecMehs2DTextures.push_back(sTextureBindBlendInfo(line, 1.0f));
				::g_pTextureManager->Create2DTextureFromBMPFile(line, true);
				continue;
			}
			if (line.find("discard: ") != std::string::npos)
			{
				if (line.find("true") != std::string::npos)
					::g_vecGameObjects.back()->bDiscardTexture = true;
			}
			if (line.find("transparent: ") != std::string::npos)
			{
				if (line.find("true") != std::string::npos)
					::g_vecGameObjects.back()->bTransparentTexture = true;
			}
			if (line.find("skybox: ") != std::string::npos)
			{
				if (line.find("true") != std::string::npos)
				{
					::g_vecGameObjects.back()->isSkybox = true;
					::g_vecGameObjects.back()->vecMeshes[0].bIsSkyBoxObject = true;
				}
			}
			if (!::g_vecGameObjects.back()->isSkybox)
			{
				if (line.find("texture1Strength:") != std::string::npos) {
					line.replace(0, 18, "");
					::g_vecGameObjects.back()->vecMeshes[0].vecMehs2DTextures[0].blendRatio = stof(line);
					//::g_vecGameObjects.back()->textureBlend[0] = stof(line);
					continue;
				}
				if (line.find("texture2Strength:") != std::string::npos) {
					line.replace(0, 18, "");
					::g_vecGameObjects.back()->vecMeshes[0].vecMehs2DTextures[1].blendRatio = stof(line);
					//::g_vecGameObjects.back()->textureBlend[1] = stof(line);
					continue;
				}
			}
			if (line == "Light Start") {
				::g_pLightManager->CreateLights(1, true);
				lightIndex++;
				continue;
			}

			if (line.find("filename:") != std::string::npos) {
				line.replace(0, 10, "");
				::g_vecGameObjects.back()->vecMeshes[0].name = line;
				::g_vecGameObjects.back()->meshName = line;
				continue;
			}
			if (line.find("physics:") != std::string::npos) {
				line.replace(0, 9, "");
				::g_vecGameObjects.back()->bIsUpdatedInPhysics = line == "true";
				continue;
			}
			if (line.find("wireframe:") != std::string::npos) {
				line.replace(0, 11, "");
				::g_vecGameObjects.back()->bIsWireFrame = line == "true";
				continue;
			}
			if (line.find("position: ") != std::string::npos) {
				line.replace(0, 10, "");

				glm::vec3 position;
				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						position.x = stof(number);
						number = "";
					}
					else
					{
						position.y = stof(number);
						number = "";
					}
				}
				position.z = stof(number);


				if (::g_vecGameObjects.back()->bIsUpdatedInPhysics)
				{
					nPhysics::sRigidBodyDesc desc;
					desc.Position = position;
					nPhysics::iShape* shape;
					if (::g_vecGameObjects.back()->typeOfObject == eTypeOfObject::SPHERE)
					{
						shape = gPhysicsFactory->CreateSphere(::g_vecGameObjects.back()->scale);
					}
					else if (::g_vecGameObjects.back()->typeOfObject == eTypeOfObject::PLANE)
					{
						shape = gPhysicsFactory->CreatePlane(normal, 1);
					}


					//gPhysicsFactory->CreateRigidBody(desc, shape);
					::g_vecGameObjects.back()->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);
					if (::g_vecGameObjects.back()->typeOfObject == eTypeOfObject::PLANE)
					{
						::g_vecGameObjects.back()->rBody->SetStatic(true);
					}


					::gPhysicsWorld->AddBody(::g_vecGameObjects.back()->rBody);
				}
				else
				{
					::g_vecGameObjects.back()->position = position;
				}
				//::g_vecGameObjects.back()->position.z = stof(number);
				continue;
			}
			if (line.find("orientation: ") != std::string::npos) {
				line.replace(0, 13, "");

				std::string number;
				bool xValue = true;
				glm::vec3 orientation = glm::vec3(0, 0, 0);
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						orientation.x = glm::radians(stof(number));
						number = "";
					}
					else
					{
						orientation.y = glm::radians(stof(number));
						number = "";
					}
				}
				orientation.z = glm::radians(stof(number));
				::g_vecGameObjects.back()->qOrientation = glm::quat(orientation);
				continue;
			}

			if (line.find("normal: ") != std::string::npos) {
				line.replace(0, 8, "");

				std::string number;
				bool xValue = true;
				glm::vec3 orientation = glm::vec3(0, 0, 0);
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						normal.x = stof(number);
						number = "";
					}
					else
					{
						normal.y = stof(number);
						number = "";
					}
				}
				normal.z = stof(number);
				continue;
			}
			if (line.find("scale:") != std::string::npos) {
				line.replace(0, 7, "");
				::g_vecGameObjects.back()->vecMeshes[0].scale = stof(line);
				::g_vecGameObjects.back()->scale = stof(line);
				continue;
			}

			if (line.find("radius:") != std::string::npos) {
				line.replace(0, 7, "");
				::g_vecGameObjects.back()->radius = stof(line);
				continue;
			}
			if (line.find("colour:") != std::string::npos) {
				line.replace(0, 8, "");

				std::string number;
				bool rValue = true;
				bool gValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (rValue)
					{
						rValue = false;
						::g_vecGameObjects.back()->diffuseColour.x = stof(number);
						number = "";
					}
					else if (gValue)
					{
						::g_vecGameObjects.back()->diffuseColour.y = stof(number);
						number = "";
					}
					else
					{
						::g_vecGameObjects.back()->diffuseColour.z = stof(number);
						number = "";
					}
				}
				//::g_vecGameObjects.back()->diffuseColour.z = stof(number);
				::g_vecGameObjects.back()->diffuseColour.a = stof(number);
				continue;
				//change the times for the most recent media object
			}
			if (line.find("attentuation:") != std::string::npos) {
				line.replace(0, 14, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].attenuation.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].attenuation.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].attenuation.z = stof(number);
				continue;
			}
			if (line.find("position_l:") != std::string::npos) {
				line.replace(0, 12, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].position.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].position.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].position.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
			if (line.find("light_type:") != std::string::npos) {
				line.replace(0, 12, "");

				if (line == "spot")
				{
					::g_pLightManager->vecLights[lightIndex].setLightParamType(cLight::eLightType::SPOT);
				}
				continue;
				//change the times for the most recent media object
			}
			if (line.find("spot_distance:") != std::string::npos) {
				line.replace(0, 15, "");

				::g_pLightManager->vecLights[lightIndex].setLightParamDistCutOff(stof(line));

				continue;
				//change the times for the most recent media object
			}
			if (line.find("spot_inner:") != std::string::npos) {
				line.replace(0, 12, "");

				::g_pLightManager->vecLights[lightIndex].setLightParamSpotPrenumAngleInner(glm::radians(stof(line)));

				continue;
				//change the times for the most recent media object
			}
			if (line.find("spot_outer:") != std::string::npos) {
				line.replace(0, 12, "");

				::g_pLightManager->vecLights[lightIndex].setLightParamSpotPrenumAngleOuter(glm::radians(stof(line)));

				continue;
				//change the times for the most recent media object
			}
			if (line.find("direction:") != std::string::npos) {
				line.replace(0, 11, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].direction.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].direction.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].direction.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
			if (line.find("colour_l:") != std::string::npos) {
				line.replace(0, 10, "");

				std::string number;
				bool xValue = true;
				for (int stringIndex = 0; stringIndex < line.size(); stringIndex++)
				{
					if (line[stringIndex] != ',')
					{
						number += line[stringIndex];
					}
					else if (xValue)
					{
						xValue = false;
						::g_pLightManager->vecLights[lightIndex].diffuse.x = stof(number);
						number = "";
					}
					else
					{
						::g_pLightManager->vecLights[lightIndex].diffuse.y = stof(number);
						number = "";
					}
				}
				::g_pLightManager->vecLights[lightIndex].diffuse.z = stof(number);
				continue;
				//change the times for the most recent media object
			}
		}

	}
	catch (std::exception ex)
	{
		return false;
	}
	return true;
}
