#include "cSoftBody.h"
#include "cRigidBody.h"
#include "shapes.h"

//ctor
nPhysics::cSoftBody::cSpring::cSpring(cNode * nodeA, cNode * nodeB)
{

	this->restingSeperation = glm::distance(nodeA->body->position, nodeB->body->position);
	this->currentSeperation = this->restingSeperation;
	this->springConstantK = 2.5f;
	this->dampingCoefficient = 2.0f;
	this->nodeA = nodeA;
	this->nodeB = nodeB;
	this->seperation = nodeA->body->position - nodeB->body->position;

}

//updates the force being applied to the nodes
void nPhysics::cSoftBody::cSpring::Update()
{
	seperation = nodeA->body->position - nodeB->body->position;
	glm::vec3 relativevel = nodeA->body->velocity - nodeB->body->velocity;
	currentSeperation = glm::length(seperation);
	glm::normalize(seperation);

	nodeA->springForce += -springConstantK *(currentSeperation - restingSeperation)*(seperation)-(dampingCoefficient*relativevel);
	nodeB->springForce += -springConstantK *(currentSeperation - restingSeperation)*(-seperation) - (dampingCoefficient*-relativevel);
}

//gets the other node attached
nPhysics::cSoftBody::cNode * nPhysics::cSoftBody::cSpring::GetOther(cNode * me)
{
	return me == nodeA ? nodeB : nodeA;
}

//check is a node already has a connection to this node
bool nPhysics::cSoftBody::cNode::HasConnection(cNode * otherNode)
{

	std::vector<cSpring*>::iterator it = otherNode->springs.begin();

	while (it != otherNode->springs.end())
	{
		if ((*it)->GetOther(this) == otherNode)
		{
			return true; // found this node
		} // end if
		++it;
	} // end while
	return false; // went through all of them, it wasnt there
}

//add a connection from one node to another
void nPhysics::cSoftBody::cNode::AddConnection(cNode * otherNode)
{
	if (HasConnection(otherNode))
		return;
	this->springs.push_back(new cSpring(this, otherNode));
}

//ctor
nPhysics::cSoftBody::cSoftBody(std::vector<std::pair<glm::vec3, glm::vec3>> pairPos, std::vector<glm::vec3> nodePos)
{
	this->maxXYZ = glm::vec3(0);
	this->minXYZ = glm::vec3(0);
	int size = nodePos.size();

	for (int i = 0; i < size; ++i)
	{
		cNode* newNode = new cNode();
		newNode->springForce = glm::vec3(0);

		sRigidBodyDesc desc;
		desc.Position = nodePos[i];
		desc.Mass = 1;
		nPhysics::iShape* shape;
		shape = new cSphereShape(1.0f);

		cRigidBody* pRigidBody = new cRigidBody(desc, shape);

		newNode->body = pRigidBody;

		mNodes.push_back(newNode);
	}


	int pairSize = pairPos.size();
	for (int i = 0; i < pairSize; ++i)
	{
		int firstNodeID = -1;
		int secondNodeID = -1;

		for (int j = 0; j < size; ++j)
		{
			if (mNodes[j]->body->position == pairPos[i].first)
			{
				firstNodeID = j;
			}

			if (mNodes[j]->body->position == pairPos[i].second)
			{
				secondNodeID = j;
			}

			if (firstNodeID != -1 && secondNodeID != -1)
			{

				mNodes[firstNodeID]->AddConnection(mNodes[secondNodeID]);
				mNodes[secondNodeID]->AddConnection(mNodes[firstNodeID]);
				break;
			}
		}
	}

	for (int index = 0; index != mNodes.size(); index++)
	{
		for (int conIndex = 0; conIndex != mNodes[index]->springs.size(); conIndex++)
		{
			this->mSprings.push_back(mNodes[index]->springs[conIndex]);
		}
	}

	mNodes[mNodes.size() - 1]->isStatic = true;
	mNodes[mNodes.size() - 10]->isStatic = true;
}

//returns the number of nodes the soft body has
size_t nPhysics::cSoftBody::NumNodes()
{
	return mNodes.size();
}

//returns the position of a certain node
void nPhysics::cSoftBody::GetNodePosition(int index, glm::vec3 & nodePositionOut)
{
	nodePositionOut = mNodes[index]->body->position;
}

//tests if a position is within the bounding box of a soft body
bool nPhysics::cSoftBody::PossiblyColliding(glm::vec3 position, float radius)
{
	glm::vec3 tempMax = this->maxXYZ + glm::vec3(radius, radius, radius);
	glm::vec3 tempMin = this->minXYZ - glm::vec3(radius, radius, radius);

	if (position.x <= tempMax.x && position.y <= tempMax.y && position.z <= tempMax.z
		&& position.y >= tempMin.y && position.z >= tempMin.z)
		return true;
	return false;
}
