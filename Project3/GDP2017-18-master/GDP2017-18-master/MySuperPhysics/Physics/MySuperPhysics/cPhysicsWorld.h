#pragma once
#include <iPhysicsWorld.h>
#include <vector>
#include "cRigidBody.h"
#include "cSoftBody.h"
#include "cIntegrator.h"

namespace nPhysics
{
	//This class will contain all the bodies in the world and run the simulation
	class cPhysicsWorld : public iPhysicsWorld
	{
	public:
		//dtor
		virtual ~cPhysicsWorld();

		//stepping forward the simulation
		virtual void TimeStep(float deltaTime);

		//adding a body to the world
		virtual void AddBody(iCollisionBody* rigidBody);
		//removing a body from the world
		virtual void RemoveBody(iCollisionBody* rigidBody);

		//setting a debug renderer for the world
		virtual void SetDebugRenderer(iDebugRenderer* debugRenderer);
		//render all the bodies in debug
		virtual void RenderDebug();

		//colliding rigid bodies
		virtual void Collide(iRigidBody* first, iRigidBody* second);
		//colliding a soft body and a rigid bodies
		virtual void Collide(cSoftBody* first, iRigidBody* second);

		//unused method that collides a node and sphere, basically only affects the node
		virtual void CollideSphereNode(iRigidBody * first, iRigidBody * second);

		//colliding a sphere with a sphere
		virtual void CollideSphereSphere(iRigidBody* first, iRigidBody* second);
		//colliding a sphere with a plane
		virtual void CollideSpherePlane(iRigidBody* first, iRigidBody* second);

	private:
		
		//data members
		std::vector<iCollisionBody*> collisionBodies;
		float deltaTime;
		cIntegrator integrator;
		double totalTime;
	};
}