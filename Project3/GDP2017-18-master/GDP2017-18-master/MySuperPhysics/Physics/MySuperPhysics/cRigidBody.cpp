#include "cRigidBody.h"

namespace nPhysics
{
	//ctor
	cRigidBody::cRigidBody(const sRigidBodyDesc& desc, iShape* shape)
		: shape(shape)
		, position(desc.Position)
		, velocity(desc.Velocity)
		, mass(desc.Mass)
		, rotation(desc.Rotation)
		, isStatic(false)
		, acceleration(glm::vec3(0,0,0))
		, orientationVelocity(glm::quat())
		, colliding(false)
		
	{
		orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
	}

	//dtor
	cRigidBody::~cRigidBody()
	{

	}

	//returns the shape of the object
	iShape* cRigidBody::GetShape()
	{
		return shape;
	}

	//returns the transform of the object
	void cRigidBody::GetTransform(glm::mat4& transformOut)
	{
		transformOut = glm::mat4_cast(rotation);
		transformOut[3][0] = position.x;
		transformOut[3][1] = position.y;
		transformOut[3][2] = position.z;
		transformOut[3][3] = 1.f;
	}

	//returns the position of the body
	void cRigidBody::GetPosition(glm::vec3& positionOut)
	{
		positionOut = position;
	}

	//returns the rotation of the body
	void cRigidBody::GetRotation(glm::vec3& rotationOut)
	{
		rotationOut = glm::eulerAngles(rotation);
	}

	//sets if the object is static or not
	void cRigidBody::SetStatic(bool isStat)
	{
		isStatic = isStat;
	}

	//returns the velocity of the body
	void cRigidBody::GetVelocity(glm::vec3 & velocityOut)
	{
		velocityOut = this->velocity;
	}

	//pushes the object with a forcce
	void cRigidBody::Push(glm::vec3 accel)
	{
		this->acceleration = accel;
	}

	//returns the orientation of the body
	void cRigidBody::GetOrientation(glm::quat & orientation)
	{
		orientation = this->orientation;
	}

	//returns if the object is colliding or not
	void cRigidBody::GetColliding(bool & colliding)
	{
		colliding = this->colliding;
	}
}