#pragma once
#include <iPhysicsFactory.h>

namespace nPhysics
{
	//This class is used to create objects needed in the simulation
	class cPhysicsFactory : public iPhysicsFactory
	{
	public:
		//dtor
		virtual ~cPhysicsFactory();

		//creating a world
		virtual iPhysicsWorld* CreateWorld();

		//creating a rigid body
		virtual iRigidBody* CreateRigidBody(const sRigidBodyDesc& desc, iShape* shape);

		//creating a soft body
		virtual iSoftBody* CreateSoftBody(iShape* shape, std::vector<std::pair<glm::vec3, glm::vec3>> pairPos, std::vector<glm::vec3> nodePos);

		//creating a sphere shape
		virtual iShape* CreateSphere(float radius);
		//creating a plane shape
		virtual iShape* CreatePlane(const glm::vec3& normal, float planeConst);

	};
}