#include "shapes.h"

namespace nPhysics
{
	//ctor
	cSphereShape::cSphereShape(float radius)
		: iShape(SHAPE_TYPE_SPHERE)
		, radius(radius)
	{

	}

	//ctor
	cSphereShape::cSphereShape()
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}

	//ctor
	cSphereShape::cSphereShape(const cSphereShape& other)
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}
	//ctor
	cSphereShape& cSphereShape::operator=(const cSphereShape& other)
	{
		return *this;
	}
	//dtor
	cSphereShape::~cSphereShape()
	{

	}
	//returns the radius of the sphere
	bool cSphereShape::GetSphereRadius(float& radiusOut)
	{
		radiusOut = radius;
		return true;
	}
	//ctor
	cPlaneShape::cPlaneShape(const glm::vec3& normal, float planeConst)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{

	}
	//ctor
	cPlaneShape::cPlaneShape()
		: iShape(SHAPE_TYPE_PLANE)
	{
	}
	//ctor
	cPlaneShape::cPlaneShape(const cPlaneShape& other)
		: iShape(SHAPE_TYPE_PLANE)
	{

	}
	//ctor
	cPlaneShape& cPlaneShape::operator=(const cPlaneShape& other)
	{
		return *this;
	}
	//dtor
	cPlaneShape::~cPlaneShape()
	{

	}
	//returns the plane normal
	bool cPlaneShape::GetPlaneNormal(glm::vec3& normalOut)
	{
		normalOut = normal;
		return true;
	}
	//returns the plane const
	bool cPlaneShape::GetPlaneConst(float& planeConstOut)
	{
		planeConstOut = planeConst;
		return true;
	}
}