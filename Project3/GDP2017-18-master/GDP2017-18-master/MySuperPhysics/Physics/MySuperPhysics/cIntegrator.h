#ifndef cINTEGRATOR_HG
#define cINTEGRATOR_HG
#include <glm\vec3.hpp>

//This struct is used to contain the state of an object, used in integration
struct State
{
	State() {}
	State(glm::vec3 pos, glm::vec3 vel, glm::vec3 accel) : position(pos), velocity(vel), acceleration(accel) {}
	glm::vec3 position;
	glm::vec3 velocity;
	glm::vec3 acceleration;
	
};

//This struct is used in combination with the State struct during integration
struct StateDerivative
{
	glm::vec3 positionDerv;
	glm::vec3 velocityDerv;
};

//This class handles the integration of all the rigid bodies in the scene
class cIntegrator
{
public:

	//ctor
	cIntegrator();
	//dtor
	~cIntegrator();

	//the acceleration of the object
	glm::vec3 acceleration(const State & state, double t);

	//evaluate function determining the new state of an object
	StateDerivative evaluate(const State & initial, double t, float dt, const StateDerivative & d);
	StateDerivative evaluate(const State & initial, float dt, const StateDerivative & d);

	//using the evaluate function to determine the velocity and position changes of a rigid body
	void integrate(State & state, double t, float dt);
	void integrate(State & state, float dt);
};



#endif // !cINTEGRATOR_HG

