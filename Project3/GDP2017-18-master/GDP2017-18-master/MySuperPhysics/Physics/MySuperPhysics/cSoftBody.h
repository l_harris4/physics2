#pragma once

#include <sSoftBodyDesc.h>
#include <iSoftBody.h>

namespace nPhysics
{
	class cRigidBody;
	//this class is used to represent a soft body in the physics world
	class cSoftBody : public iSoftBody
	{
	private:
		class cNode;
		//The following class will apply force to nodes to try a keep a resting distance between them
		class cSpring
		{
		public:
			//ctor
			cSpring(cNode* nodeA, cNode* nodeB);
			void Update();

			cNode* GetOther(cNode* me);


			float restingSeperation;
			float currentSeperation;
			float springConstantK;
			float dampingCoefficient;
			glm::vec3 seperation;
			
			
			//the two nodes on either side of the spring
			cNode* nodeA;
			cNode* nodeB;
		};
		//The following class represents part of a soft body
		//nodes will contains rigid bodies
		//nodes will be pushed apart by spring objects
		class cNode
		{
		public: 
			bool HasConnection(cNode* otherNode);
			void AddConnection(cNode* otherNode);
			std::vector<cSpring*> springs;
			cRigidBody* body;

			bool isStatic;
			bool collidedThisFrame = false;
			glm::vec3 springForce;
		};
	public:
		//ctor
		cSoftBody(std::vector<std::pair<glm::vec3, glm::vec3>> pairPos, std::vector<glm::vec3> nodePos);
		//returns the total number of nodes
		size_t NumNodes();
		//gets the position of a specific node
		void GetNodePosition(int index, glm::vec3& nodePositionOut);
		//see if something could potentially be colliding with the rigid body
		bool PossiblyColliding(glm::vec3 position, float radius);

	protected:
		//data members
		std::vector<cNode*> mNodes;
		std::vector<cSpring*> mSprings;
		glm::vec3 maxXYZ;
		glm::vec3 minXYZ;

		friend class cPhysicsWorld;
	};
}