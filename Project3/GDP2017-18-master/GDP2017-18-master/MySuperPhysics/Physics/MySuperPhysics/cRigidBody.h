#pragma once
#include <iRigidBody.h>
#include <sRigidBodyDesc.h>

namespace nPhysics
{
	class cPhysicsWorld;
	//this class is used to represent a rigid body in the physics world
	class cRigidBody : public iRigidBody
	{
	public:
		//ctor
		cRigidBody(const sRigidBodyDesc& desc, iShape* shape);
		//dtor
		virtual ~cRigidBody();

		//gets the shape of the object
		virtual iShape* GetShape();

		//returns the transform of the object
		virtual void GetTransform(glm::mat4& transformOut);
		//returns the position of the body
		virtual void GetPosition(glm::vec3& positionOut);
		//returns the rotation of the body
		virtual void GetRotation(glm::vec3& rotationOut);
		//sets if the object is static or not
		virtual void SetStatic(bool isStat);
		//returns the velocity of the body
		virtual void GetVelocity(glm::vec3& velocityOut);
		//pushes the object with a forcce
		virtual void Push(glm::vec3 accel);
		//returns the orientation of the body
		virtual void GetOrientation(glm::quat &orientation);
		//returns if the object is colliding or not
		virtual void GetColliding(bool& colliding);
		bool isStatic;

	private:
		friend class cPhysicsWorld;
		friend class cSoftBody;

		//data members
		iShape* shape;
		glm::vec3 position;
		glm::vec3 positionOld;
		glm::vec3 velocity;
		glm::vec3 velocityOld;
		glm::quat rotation;
		glm::vec3 acceleration;
		glm::quat orientation;
		glm::quat orientationVelocity;
		float mass;
		bool colliding;
	};
}