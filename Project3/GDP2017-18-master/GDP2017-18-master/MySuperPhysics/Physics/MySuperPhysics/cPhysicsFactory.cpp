#include "cPhysicsFactory.h"
#include "cRigidBody.h"
#include "shapes.h"
#include "cPhysicsWorld.h"
#include "cSoftBody.h"

namespace nPhysics
{
	//dtor
	cPhysicsFactory::~cPhysicsFactory() {}

	//creating the physics world
	iPhysicsWorld* cPhysicsFactory::CreateWorld()
	{
		return new cPhysicsWorld();
	}

	//creating a rigid body
	iRigidBody* cPhysicsFactory::CreateRigidBody(const sRigidBodyDesc& desc, iShape* shape)
	{
		return new cRigidBody(desc, shape);
	}

	//creating a soft body
	iSoftBody * cPhysicsFactory::CreateSoftBody(iShape * shape, std::vector<std::pair<glm::vec3, glm::vec3>> pairPos, std::vector<glm::vec3> nodePos)
	{
		cSoftBody* softBody = new cSoftBody(pairPos, nodePos);
		return softBody;
	}

	//creating a sphere shape
	iShape* cPhysicsFactory::CreateSphere(float radius)
	{
		return new cSphereShape(radius);
	}

	//creating a plane shape
	iShape* cPhysicsFactory::CreatePlane(const glm::vec3& normal, float planeConst)
	{
		return new cPlaneShape(normal, planeConst);
	}


	
}