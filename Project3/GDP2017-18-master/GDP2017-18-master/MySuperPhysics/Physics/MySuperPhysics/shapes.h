#pragma once
#include <glm\game_math.h>
#include <iShape.h>

namespace nPhysics
{
	//a class to keep track of a sphere shape
	class cSphereShape : public iShape
	{
	public:
		//ctor
		cSphereShape(float radius);

		//dtor
		virtual ~cSphereShape();

		//returns the radius of a sphere
		virtual bool GetSphereRadius(float& radiusOut);

		//ctor
		cSphereShape();
		//copy ctor
		cSphereShape(const cSphereShape& other);
		//copy constructor
		cSphereShape& operator=(const cSphereShape& other);

		//the radius of the sphere
		float radius;
	};

	//a class representing an infinite plan
	class cPlaneShape : public iShape
	{
	public:
		//ctor
		cPlaneShape(const glm::vec3& normal, float planeConst);
		
		//dtor
		virtual ~cPlaneShape();

		//returns the normal of the plane
		virtual bool GetPlaneNormal(glm::vec3& normalOut);
		//returns the plane const
		virtual bool GetPlaneConst(float& planeConstOut);

	private:
		//ctor
		cPlaneShape();
		//copy constructor
		cPlaneShape(const cPlaneShape& other);
		//copy constructor
		cPlaneShape& operator=(const cPlaneShape& other);

		//data members
		glm::vec3 normal;
		float planeConst;
	};
}