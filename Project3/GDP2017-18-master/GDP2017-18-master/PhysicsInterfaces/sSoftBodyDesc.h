#pragma once

#include <glm/game_math.h>
#include <vector>


namespace nPhysics
{
	struct sSoftBodyDes
	{
		std::vector<glm::vec3> vertices;
		std::vector<size_t> trianglulatedIndices;
		std::vector<size_t> staticIndices;
	};
}