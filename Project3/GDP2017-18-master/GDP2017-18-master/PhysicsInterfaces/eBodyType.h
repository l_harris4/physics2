#ifndef _eBodyType_HG_
#define _eBodyType_HG_

namespace nPhysics
{
	enum eBodyType
	{
		RIGID_BODY_TYPE = 1,
		SOFT_BODY_TYPE = 2
	};
}

#endif
