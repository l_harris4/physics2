#pragma once
#include "iRigidBody.h"
#include "iDebugRenderer.h"
class iCollisionBody;

namespace nPhysics
{
	class iPhysicsWorld
	{
	public:
		virtual ~iPhysicsWorld() {}

		virtual void TimeStep(float deltaTime) = 0;

		virtual void AddBody(iCollisionBody* rigidBody) = 0;
		virtual void RemoveBody(iCollisionBody* rigidBody) = 0;

		virtual void SetDebugRenderer(iDebugRenderer* debugRenderer) = 0;
		virtual void RenderDebug() = 0;
		//virtual void Collide(iRigidBody* first, iRigidBody* second) = 0;
	};
}