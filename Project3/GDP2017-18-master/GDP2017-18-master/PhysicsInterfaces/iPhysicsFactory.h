#pragma once
#include "iRigidBody.h"
#include "sRigidBodyDesc.h"
#include "iShape.h"
#include "iPhysicsWorld.h"
#include "iSoftBody.h"
#include <vector>


namespace nPhysics
{
	class iPhysicsFactory
	{
	public:
		virtual ~iPhysicsFactory() {}

		virtual iPhysicsWorld* CreateWorld() = 0;

		virtual iRigidBody* CreateRigidBody(const sRigidBodyDesc& desc, iShape* shape) = 0;
		virtual iSoftBody* CreateSoftBody(iShape* shape, std::vector<std::pair<glm::vec3, glm::vec3>> pairPos, std::vector<glm::vec3> nodePos) = 0;
		//virtual iSoftBody* CreateSoftBody(iShape* shape, std::vector<glm::vec3> nodePos) = 0;
		virtual iShape* CreateSphere(float radius) = 0;
		virtual iShape* CreatePlane(const glm::vec3& normal, float planeConst) = 0;
	};
}