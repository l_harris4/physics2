#ifndef _iCollisionBody_HG_
#define _iCollisionBody_HG_

#include "eBodyType.h"

namespace nPhysics
{
	// base class/interface for all bodies
	class iCollisionBody
	{
	public:
		virtual ~iCollisionBody() {}

		// eBodyType
		inline eBodyType GetBodyType() const { return mBodyType; }


	protected:
		// protected constructors
		// so nothing can create this object exept for a subclass
		iCollisionBody(eBodyType bodyType) : mBodyType(bodyType) {}
		iCollisionBody(const iCollisionBody& other) {}
		iCollisionBody& operator=(const iCollisionBody& other) { return *this; }
	private:
		eBodyType mBodyType;
	};
}

#endif

