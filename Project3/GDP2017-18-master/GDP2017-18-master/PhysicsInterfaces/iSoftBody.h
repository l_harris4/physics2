#pragma once

#include <glm/game_math.h>
#include "iCollisionBody.h"

namespace nPhysics
{
	class iSoftBody : public iCollisionBody
	{
	public:
		virtual ~iSoftBody() {}
		virtual void GetNodePosition(int index, glm::vec3& nodePositionOut) = 0;
		virtual size_t NumNodes() = 0;
		//virtual void GetAABB(glm::vec3& min, glm::vec3& max) = 0;
	protected:
		iSoftBody() : iCollisionBody(SOFT_BODY_TYPE) {}
	};
}
