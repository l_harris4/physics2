#include "shapes.h"

namespace nPhysics
{
	cSphereShape::cSphereShape(float radius)
		: iShape(SHAPE_TYPE_SPHERE)
		, radius(radius)
	{

	}
	cSphereShape::cSphereShape()
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}

	cSphereShape::cSphereShape(const cSphereShape& other)
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}
	cSphereShape& cSphereShape::operator=(const cSphereShape& other)
	{
		return *this;
	}
	cSphereShape::~cSphereShape()
	{

	}
	bool cSphereShape::GetSphereRadius(float& radiusOut)
	{
		radiusOut = radius;
		return true;
	}
	cPlaneShape::cPlaneShape(const glm::vec3& normal, float planeConst)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{

	}
	cPlaneShape::cPlaneShape()
		: iShape(SHAPE_TYPE_PLANE)
	{
	}

	cPlaneShape::cPlaneShape(const cPlaneShape& other)
		: iShape(SHAPE_TYPE_PLANE)
	{

	}
	cPlaneShape& cPlaneShape::operator=(const cPlaneShape& other)
	{
		return *this;
	}
	cPlaneShape::~cPlaneShape()
	{

	}
	bool cPlaneShape::GetPlaneNormal(glm::vec3& normalOut)
	{
		normalOut = normal;
		return true;
	}
	bool cPlaneShape::GetPlaneConst(float& planeConstOut)
	{
		planeConstOut = planeConst;
		return true;
	}
}