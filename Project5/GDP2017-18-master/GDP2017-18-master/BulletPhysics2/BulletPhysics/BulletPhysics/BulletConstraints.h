#ifndef BULLETCONSTRAINT_HG
#define BULLETCONSTRAINT_HG

#include <iConstraint.h>
#include "../BulletPhysics/BulletDynamics/ConstraintSolver/btPoint2PointConstraint.h"
#include "../BulletPhysics/BulletDynamics/ConstraintSolver/btHingeConstraint.h"
#include "../BulletPhysics/BulletDynamics/ConstraintSolver/btFixedConstraint.h"

namespace nPhysics
{
	//forward declares
	class cBulletRigidBody;
	class cBulletPhysicsWorld;

	//generic bullet constraint class
	class cBulletConstraint : public iConstraint
	{
	public:
		//dtor
		virtual ~cBulletConstraint()
		{
			delete constraint;
		}

		friend class cBulletPhysicsWorld;
		//ctor
		cBulletConstraint(eConstraintType constraintType, btTypedConstraint* constraintIn) : iConstraint(constraintType), constraint(constraintIn)
		{
			constraint->setUserConstraintPtr(0);
		}

		btTypedConstraint* constraint;
	};

	//ball and socket constraint
	class cBallAndSocketConstraint : public cBulletConstraint
	{
	public:
		//ctors
		cBallAndSocketConstraint(cBulletRigidBody* rb, const btVector3& pivot);
		cBallAndSocketConstraint(cBulletRigidBody* rbA, const btVector3& pivotA, cBulletRigidBody* rgB, const btVector3& pivotB);
		//dtor
		virtual ~cBallAndSocketConstraint();
	};

	//6 degrees of freedom constraint
	class c6DegreesOfFreedomConstraint : public cBulletConstraint
	{
	public:
		//ctor
		c6DegreesOfFreedomConstraint::c6DegreesOfFreedomConstraint(cBulletRigidBody * rb, const btTransform & rbAFrame);
		//ctor
		c6DegreesOfFreedomConstraint::c6DegreesOfFreedomConstraint(cBulletRigidBody * rb, const btTransform & rbAFrame, btVector3 pos);
		//dtor
		virtual ~c6DegreesOfFreedomConstraint();
	};

	//hinge constraint class
	class cHingeConstraint : public cBulletConstraint
	{
	public:
		//ctor
		cHingeConstraint(cBulletRigidBody* rb, const btVector3& pivot, const btVector3& axis, float lowLimit, float highLimit);
		//dtor
		virtual ~cHingeConstraint();
	};

	//slider constraint class
	class cSliderConstraint : public cBulletConstraint
	{
	public:
		//ctor
		cSliderConstraint(cBulletRigidBody* rb, const btTransform& frameInB);
		//dtor
		virtual ~cSliderConstraint();
	};

	//fixed constraint
	class cFixedConstraint : public cBulletConstraint
	{
	public:
		//ctor
		cFixedConstraint(cBulletRigidBody* rbA, cBulletRigidBody* rbB, const btTransform& frameInA, const btTransform& frameInB);
		//dtor
		virtual ~cFixedConstraint();
	};

	//gear and cone constraint classes not used/////////////////////////////////////////////////
	class cGearConstraint : public cBulletConstraint
	{
	public:
		cGearConstraint(cBulletRigidBody* rbA, cBulletRigidBody* rbB, const btVector3& axisInA, const btVector3& axisInB, btScalar ratio = 1.0f);
		virtual ~cGearConstraint();
	};

	class cConeConstraint : public cBulletConstraint
	{
	public:
		cConeConstraint(cBulletRigidBody* rbA, cBulletRigidBody* rbB, const btTransform& rbAFrame, const btTransform& rbBframe);
		virtual ~cConeConstraint();
	};
}
#endif // !BULLETCONSTRAINT_HG

