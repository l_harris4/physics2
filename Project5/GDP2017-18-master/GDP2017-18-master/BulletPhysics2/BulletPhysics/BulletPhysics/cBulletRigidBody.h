#pragma once
#include <iRigidBody.h>
#include <sRigidBodyDesc.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace nPhysics
{
	class cPhysicsWorld;
	//class to represent a rigid body in the world
	class cBulletRigidBody : public iRigidBody
	{
	public:
		//ctor
		cBulletRigidBody(const sRigidBodyDesc& desc, iShape* shape);
		//dtor
		virtual ~cBulletRigidBody();

		virtual iShape* GetShape();

		//getters and setters
		virtual void GetTransform(glm::mat4& transformOut);
		virtual void GetPosition(glm::vec3& positionOut);
		virtual void GetRotation(glm::vec3& rotationOut);
		virtual void SetStatic(bool isStat);
		virtual void GetVelocity(glm::vec3& velocityOut);
		virtual void SetVelocity(glm::vec3& velocityIn);
		virtual void Push(glm::vec3 accel);
		virtual void GetOrientation(glm::quat &orientation);
		virtual void SetOrientation(glm::vec3 rotation);
		virtual void GetColliding(bool& colliding);
		virtual void SetColliding(bool colliding);
		bool isStatic;

		//data members
		btRigidBody* body;
		btMotionState* motionState;
		btCollisionShape* collisionShape;
		btTransform* transform;

	private:
		friend class cBulletPhysicsWorld;
		iShape* shape;
		glm::vec3 position;
		glm::vec3 positionOld;
		glm::vec3 velocity;
		glm::vec3 velocityOld;
		glm::quat rotation;
		glm::vec3 acceleration;
		glm::quat orientation;
		glm::quat orientationVelocity;
		float mass;
		bool colliding;
		float collidingCounter;


		
	};
}