#pragma once
#include <glm\game_math.h>
#include <iShape.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace nPhysics
{
	//a sphere shape
	class cBulletSphereShape : public iShape
	{
	public:
		//The ctor for intented use
		cBulletSphereShape(float radius);

		//dtor
		virtual ~cBulletSphereShape();
		//legacy method
		virtual bool GetSphereRadius(float& radiusOut);
		btCollisionShape* bulletShape;
	private:
		//ctors
		cBulletSphereShape();
		cBulletSphereShape(const cBulletSphereShape& other);
		cBulletSphereShape& operator=(const cBulletSphereShape& other);

		float radius;

	};

	//a plane shape
	class cBulletPlaneShape : public iShape
	{
	public:
		//ctors
		cBulletPlaneShape(const glm::vec3& normal, float planeConst, glm::vec3& pointOnPlane);
		cBulletPlaneShape(const glm::vec3& normal, float planeConst);
		//dtor
		virtual ~cBulletPlaneShape();

		//getters
		virtual bool GetPlaneNormal(glm::vec3& normalOut);
		virtual bool GetPlaneConst(float& planeConstOut);
		//the actual shape in bullet
		btCollisionShape* bulletShape;
		//data members
		glm::vec3 normal;
		float planeConst;
	private:
		//ctors
		cBulletPlaneShape();
		cBulletPlaneShape(const cBulletPlaneShape& other);
		cBulletPlaneShape& operator=(const cBulletPlaneShape& other);

		
	};

	//a box shape
	class cBulletBoxShape : public iShape
	{
	public:
		//ctor
		cBulletBoxShape(glm::vec3 boxHalfWidths);
		//dtor
		virtual ~cBulletBoxShape();
		//the actual bullet shape
		btCollisionShape* bulletShape;
	private:
		//ctors
		cBulletBoxShape();
		cBulletBoxShape(const cBulletBoxShape& other);
		cBulletBoxShape& operator=(const cBulletBoxShape& other);
	};

	//cylinder shape
	class cBulletCylinderShape : public iShape
	{
	public:
		//ctor
		cBulletCylinderShape(glm::vec3 boxHalfWidths);
		//dtor
		virtual ~cBulletCylinderShape();
		//the actual bullet shape
		btCollisionShape* bulletShape;
	private:
		//ctors
		cBulletCylinderShape();
		cBulletCylinderShape(const cBulletCylinderShape& tempCylinder);
		cBulletCylinderShape& operator=(const cBulletCylinderShape& other);
	};

	//a cone shape
	class cBulletConeShape : public iShape
	{
	public:
		//ctor
		cBulletConeShape(float height, float radius);
		//dtor
		virtual ~cBulletConeShape();
		//the actual bullet shape
		btCollisionShape* bulletShape;
	private:
		//ctors
		cBulletConeShape();
		cBulletConeShape(const cBulletConeShape& tempCylinder);
		cBulletConeShape& operator=(const cBulletConeShape& other);
	};

	//a capsule shape
	class cBulletCapsuleShape : public iShape
	{
	public:
		//ctor
		cBulletCapsuleShape(float radius, float height);
		//dtor
		virtual ~cBulletCapsuleShape();
		//the actual bullet shape
		btCollisionShape* bulletShape;
	private:
		//ctors
		cBulletCapsuleShape();
		cBulletCapsuleShape(const cBulletCapsuleShape& tempCylinder);
		cBulletCapsuleShape& operator=(const cBulletCapsuleShape& other);
	};
}