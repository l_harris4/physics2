#pragma once
#include <iPhysicsWorld.h>
#include <vector>
#include "cBulletRigidBody.h"
#include "cBulletIntegrator.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"
#include <iConstraint.h>

namespace nPhysics
{
	//a class to keep track of the whole physics world, containing all the rigid bodies
	class cBulletPhysicsWorld : public iPhysicsWorld
	{
	public:
		//dtor
		virtual ~cBulletPhysicsWorld();
		//constructors
		cBulletPhysicsWorld();
		cBulletPhysicsWorld(btVector3 gravity);
		cBulletPhysicsWorld(glm::vec3 max, glm::vec3 min);

		//moving the simulation forward
		virtual void TimeStep(float deltaTime);
		//used to interpret all the collisions
		void Collisions();

		//for adding a rigid body to the world
		virtual void AddRigidBody(iRigidBody* rigidBody);
		//for removing a rigid body from the world
		virtual void RemoveRigidBody(iRigidBody* rigidBody);

		virtual void SetDebugRenderer(iDebugRenderer* debugRenderer);
		virtual void RenderDebug();
		//collide functions, not used but needed for the interface
		virtual void Collide(iRigidBody* first, iRigidBody* second);
		virtual void CollideSphereSphere(iRigidBody* first, iRigidBody* second);
		virtual void CollideSpherePlane(iRigidBody* first, iRigidBody* second);

		//adding and removing constraints
		virtual bool AddConstraint(iConstraint* constraint);
		virtual bool RemoveConstraint(iConstraint* constraint);

	private:
		
		//a vector of all the rigid bodies
		std::vector<iRigidBody*> rigidBodies;
		float deltaTime;
		cBulletIntegrator integrator;
		double totalTime;

		//the max extents of the world
		glm::vec3 worldMax;
		glm::vec3 worldMin;

		//data members
		btDiscreteDynamicsWorld* dynamicsWorld;
		btAlignedObjectArray<btCollisionShape*> collisionShapes;
		btSequentialImpulseConstraintSolver* solver;
		btBroadphaseInterface* overlappingPairCache;
		btCollisionDispatcher* dispatcher;
		btDefaultCollisionConfiguration* collisionConfiguration;
		btCollisionWorld* collisionWorld;
	};
}