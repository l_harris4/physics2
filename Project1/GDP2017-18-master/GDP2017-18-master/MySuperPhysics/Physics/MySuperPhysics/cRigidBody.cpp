#include "cRigidBody.h"

namespace nPhysics
{
	cRigidBody::cRigidBody(const sRigidBodyDesc& desc, iShape* shape)
		: shape(shape)
		, position(desc.Position)
		, velocity(desc.Velocity)
		, mass(desc.Mass)
		, rotation(desc.Rotation)
		, isStatic(false)
		, acceleration(glm::vec3(0,0,0))
		, orientationVelocity(glm::quat())
		, colliding(false)
		
	{
		orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
	}
	cRigidBody::~cRigidBody()
	{

	}

	iShape* cRigidBody::GetShape()
	{
		return shape;
	}

	void cRigidBody::GetTransform(glm::mat4& transformOut)
	{
		transformOut = glm::mat4_cast(rotation);
		transformOut[3][0] = position.x;
		transformOut[3][1] = position.y;
		transformOut[3][2] = position.z;
		transformOut[3][3] = 1.f;
	}
	void cRigidBody::GetPosition(glm::vec3& positionOut)
	{
		positionOut = position;
	}
	void cRigidBody::GetRotation(glm::vec3& rotationOut)
	{
		rotationOut = glm::eulerAngles(rotation);
	}
	void cRigidBody::SetStatic(bool isStat)
	{
		isStatic = isStat;
	}
	void cRigidBody::GetVelocity(glm::vec3 & velocityOut)
	{
		velocityOut = this->velocity;
	}
	void cRigidBody::Push(glm::vec3 accel)
	{
		this->acceleration = accel;
	}
	void cRigidBody::GetOrientation(glm::quat & orientation)
	{
		orientation = this->orientation;
	}
	void cRigidBody::GetColliding(bool & colliding)
	{
		colliding = this->colliding;
	}
}