#pragma once
#include <iPhysicsWorld.h>
#include <vector>
#include "cRigidBody.h"
#include "cIntegrator.h"

namespace nPhysics
{
	class cPhysicsWorld : public iPhysicsWorld
	{
	public:
		virtual ~cPhysicsWorld();

		virtual void TimeStep(float deltaTime);

		virtual void AddRigidBody(iRigidBody* rigidBody);
		virtual void RemoveRigidBody(iRigidBody* rigidBody);

		virtual void SetDebugRenderer(iDebugRenderer* debugRenderer);
		virtual void RenderDebug();
		virtual void Collide(iRigidBody* first, iRigidBody* second);
		virtual void CollideSphereSphere(iRigidBody* first, iRigidBody* second);
		virtual void CollideSpherePlane(iRigidBody* first, iRigidBody* second);

	private:
		
		std::vector<cRigidBody*> rigidBodies;
		float deltaTime;
		cIntegrator integrator;
		double totalTime;
	};
}