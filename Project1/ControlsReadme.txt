--------------------------------------------
CONTROLS
--------------------------------------------
O: to turn debug mode on and off
 -in debug mode you should see bounding boxes of the spheres and the spheres should turn white at moment of collision
 
WASD: are the controls to move the controlled ball
Space: to change which ball is being controlled
Left and right arrow keys: to rotate around the controlled ball
Up and down arrow keys: to zoom in and out