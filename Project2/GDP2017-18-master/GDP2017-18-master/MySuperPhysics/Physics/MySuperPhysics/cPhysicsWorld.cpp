#include "cPhysicsWorld.h"
#include <algorithm>
#include <iostream>
#include "shapes.h"

void adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange, glm::quat& inOutQuat)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	inOutQuat = inOutQuat * qRotationChange;

	return;
}

namespace nPhysics
{
	//dtor
	cPhysicsWorld::~cPhysicsWorld()
	{

	}


	void cPhysicsWorld::TimeStep(float deltaTime)
	{
		this->deltaTime = deltaTime;
		this->totalTime += deltaTime;

		//a loop to go through all the rigid bodies in the world
		std::vector<cRigidBody*>::iterator itRigidBody = rigidBodies.begin();
		while (itRigidBody != rigidBodies.end())
		{
			//the current rigid body
			cRigidBody* rb = *itRigidBody;
			rb->colliding = false;

			//if the body is static then simply skip over it
			if (rb->isStatic)
			{
				itRigidBody++;
				continue;
			}

			//set the old position and old velocity before updating them
			rb->positionOld = rb->position;
			rb->velocityOld = rb->velocity;


			//create a state object to be used by the itegrator
			State state(rb->position, rb->velocity, rb->acceleration);

			//integrate
			integrator.integrate(state, deltaTime);
			//update the position and velocity
			rb->position = state.position;
			rb->velocity = state.velocity;

			//clear the acceleration
			rb->acceleration = glm::vec3(0);


			eShapeType typeA = (*itRigidBody)->GetShape()->GetShapeType();
			if (typeA == eShapeType::SHAPE_TYPE_SPHERE)
			{
				glm::vec3 angularVel = rb->velocity;

				//glm::quat q(0,
				//	angularVel.x,
				//	angularVel.y,
				//	angularVel.z);

				////spin = 0.5f * q * orientation;
				//rb->orientation += 0.5f * q;// *rb->orientation;

				//rb->orientation.w += 0.001;

				adjustQOrientationFormDeltaEuler(glm::vec3(glm::radians(angularVel.x),
					glm::radians(angularVel.y),
					glm::radians(angularVel.z)), rb->orientation);
			}

			itRigidBody++;
		}

		//go through all the rigid bodies again to check if they are colliding
		size_t numBodies = rigidBodies.size();
		for (size_t idxA = 0; idxA < numBodies - 1; idxA++)
		{
			for (size_t idxB = idxA + 1; idxB < numBodies; idxB++)
			{
				Collide(rigidBodies[idxA], rigidBodies[idxB]);
			}
		}
	}

	//add a rigid body to the scene
	void cPhysicsWorld::AddRigidBody(iRigidBody* rigidBody)
	{
		cRigidBody* rb = dynamic_cast<cRigidBody*>(rigidBody);
		if (!rb)
		{
			return;
		}
		std::vector<cRigidBody*>::iterator itRigidBody;
		itRigidBody = std::find(rigidBodies.begin(), rigidBodies.end(), rb);
		if (itRigidBody == rigidBodies.end())
		{
			rigidBodies.push_back(rb);
		}
	}

	//remove a rigid body from the scene
	void cPhysicsWorld::RemoveRigidBody(iRigidBody* rigidBody)
	{
		cRigidBody* rb = dynamic_cast<cRigidBody*>(rigidBody);
		if (!rb)
		{
			return;
		}
		std::vector<cRigidBody*>::iterator itRigidBody;

		itRigidBody = std::remove(rigidBodies.begin(), rigidBodies.end(), rb);
		if (itRigidBody != rigidBodies.end())
		{
			rigidBodies.resize(rigidBodies.size() - 1);
		}
	}


	void cPhysicsWorld::SetDebugRenderer(iDebugRenderer * debugRenderer)
	{
	}

	void cPhysicsWorld::RenderDebug()
	{
	}

	//the following 5 functions are using for collision detection and were posted for us
	inline glm::vec3 closest_point_on_plane(const glm::vec3& point, const glm::vec3& n, float d)
	{
		float t = dot(n, point) - d;
		return point - t*n;
	}

	inline int intersect_moving_sphere_plane(const glm::vec3& c, float r, const glm::vec3& v, const glm::vec3& n, float d, float& t, glm::vec3& q)
	{
		float dist = dot(n, c) - d;
		if (abs(dist) <= r)
		{
			// sphere is already overlapping the plane.
			// set time of intersection to 0 and q to sphere center
			t = 0.f;
			q = c;
			return -1;
		}
		else
		{
			float denom = dot(n, v);
			if (denom * dist >= 0.f)
			{
				// no intersection as sphere moving parallel to or away from plane
				return 0;
			}
			else
			{
				// sphere is moving towards the plane
				// use +r in computations if sphere in front of plane, else -r
				float rad = dist > 0.f ? r : -r;
				t = (r - dist) / denom;
				q = c + (v * t) - (n * rad);
				return t <= 1.f ? 1 : 0;
			}
		}
	}

	inline int intersect_ray_sphere(const glm::vec3& point, const glm::vec3& dir, const glm::vec3& center, float radius, float& t, glm::vec3& q)
	{
		glm::vec3 m = point - center;
		float b = dot(m, dir);
		float c = dot(m, m) - radius * radius;
		// exit if r's origin outside s (c > 0) and r pointing away from s (b > 0)
		if (c > 0.f && b > 0.f) return 0;
		float discr = b * b - c;
		// a negative discriminant corresponds to ray missing sphere
		if (discr < 0.f) return 0;
		// ray now found to intersect sphere, compute against smallest t value of intersection
		t = -b - sqrt(discr);
		// if t is negative, ray started inside sphere, so clamp t to zero
		if (t < 0.f) t = 0.f;
		q = point + dir * t;
		return 1;
	}

	inline int intersect_moving_sphere_sphere(
		const glm::vec3& c0, float r0, const glm::vec3& v0,
		const glm::vec3& c1, float r1, const glm::vec3& v1, float& t)
	{
		glm::vec3 s = c1 - c0;  // sphere center separation
		glm::vec3 v = v1 - v0;  // relative motion of sphere 1 w.r.t. stationary s0
		float r = r1 + r0; // sum of sphere radii
		float c = dot(s, s) - r * r;
		if (c < 0.f)
		{
			// spheres initially overlapping, exit early
			t = 0.f;
			return -1;
		}
		float a = dot(v, v);
		if (a < FLT_EPSILON) return 0; // spheres not moving relative to eachother
		float b = dot(v, s);
		if (b >= 0.f) return 0; // spheres not moving towards eachother
		float d = b * b - a * c;
		if (d < 0.f) return 0;  // no real root, so spheres do not intersect

		t = (-b - sqrt(d)) / a;
		return t < 1.f ? 1 : 0;
	}

	inline int intersect_moving_sphere_sphere_mine(
		const glm::vec3& c0, float r0, const glm::vec3& v0,
		const glm::vec3& c1, float r1, const glm::vec3& v1, float& t)
	{
		if (glm::distance(c0, c1) < (r0 + r1))
			return 1;
		else
			return 0;
	}

	//to test for and handle a collision between 2 rigid bodies
	void cPhysicsWorld::Collide(iRigidBody * first, iRigidBody * second)
	{
		eShapeType typeA = first->GetShape()->GetShapeType();
		eShapeType typeB = second->GetShape()->GetShapeType();
		switch (typeA)
		{
		case eShapeType::SHAPE_TYPE_SPHERE: //if the first shape is a sphere
		{

			cSphereShape* sphereA = dynamic_cast<cSphereShape*>(first->GetShape());
			switch (typeB)
			{
			case eShapeType::SHAPE_TYPE_SPHERE:
			{
				//do sphere sphere collision
				cSphereShape* sphereB = dynamic_cast<cSphereShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSphereSphere(first, second);
			}
			break;
			case eShapeType::SHAPE_TYPE_PLANE:
			{
				//do sphere plane collision
				cPlaneShape* planeB = dynamic_cast<cPlaneShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSpherePlane(first, second);
			}
			break;
			}
		}
		break;
		case eShapeType::SHAPE_TYPE_PLANE: //if the first shape is a plane
		{
			cPlaneShape* planeA = dynamic_cast<cPlaneShape*>(first->GetShape());
			switch (typeB)
			{
			case eShapeType::SHAPE_TYPE_SPHERE:
			{
				//do plane sphere collision
				cSphereShape* sphereB = dynamic_cast<cSphereShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSpherePlane(second, first);
			}
			break;
			}
		}
		break;
		}
	}

	void cPhysicsWorld::CollideSphereSphere(iRigidBody * first, iRigidBody * second)
	{
		//get the spheres that are possibly colliding
		cRigidBody* sphere1 = (cRigidBody*)first;
		cRigidBody* sphere2 = (cRigidBody*)second;

		//get the shapes attached to the rigid bodies
		cSphereShape* sphereShape1 = dynamic_cast<cSphereShape*>(sphere1->GetShape());
		cSphereShape* sphereShape2 = dynamic_cast<cSphereShape*>(sphere2->GetShape());

		//get the radii of the spheres
		float radius1;
		sphereShape1->GetSphereRadius(radius1);
		float radius2;
		sphereShape2->GetSphereRadius(radius2);


		float t = 0.0f;
		//test to see if the spheres are colliding
		int result = intersect_moving_sphere_sphere_mine(sphere1->position, radius1, sphere1->velocity, sphere2->position, radius2,
			sphere2->velocity, t);
		/*int result = intersect_moving_sphere_sphere(sphere1->positionOld, radius1, sphere1->velocity, sphere2->positionOld, radius2,
			sphere2->velocity, t);*/

		//get the velocities of both spheres
		glm::vec3 velocity1 = sphere1->velocity;
		glm::vec3 velocity2 = sphere2->velocity;

		//if a collision happened this frame
		if (result == 1) 
		{
			sphere1->colliding = true;
			sphere2->colliding = true;
			glm::vec3 resultingVel = glm::vec3(0.0f, 0.0f, 0.0f);

			//calculate a distance to move the objects away from eachother
			glm::vec3 distanceVector = glm::vec3(0.0f, 0.0f, 0.0f);
			glm::vec3 distanceVector2 = glm::vec3(0.0f, 0.0f, 0.0f);
			glm::vec3 n = sphere1->positionOld - sphere2->positionOld;
			glm::normalize(n);
			glm::vec3 n2 = sphere2->positionOld - sphere1->positionOld;
			glm::normalize(n2);

			float a1 = glm::dot(velocity1, n);
			float a2 = glm::dot(velocity2, n);

			float a3 = glm::dot(velocity2, n2);
			float a4 = glm::dot(velocity1, n2);

			float optimizedP = (2.0 * (a1 - a2)) / (radius1 + radius2);
			float optimizedP2 = (2.0 * (a3 - a4)) / (radius1 + radius2);

			glm::vec3 v1 = (sphere1->velocityOld - optimizedP * (radius1)* n) / (radius1 * radius1); //radius acts as mass here //divide by mass to exagerate masses affect
			glm::vec3 v2 = (sphere2->velocityOld - optimizedP2 * (radius2)* n2) /(radius2 * radius2);

			sphere1->velocity = v1*0.2f;
			sphere2->velocity = v2*0.2f;
		}
		else if (result == -1) //if the spheres are already colliding
		{
			//give the spheres velocity to push them out of eachother
			sphere1->velocity += sphere1->position - sphere2->position;
			sphere2->velocity += sphere2->position - sphere1->position;
		}
	}

	void cPhysicsWorld::CollideSpherePlane(iRigidBody * first, iRigidBody * second)
	{
		
		//get the rigid bodies
		cRigidBody* sphere = (cRigidBody*)first;
		cRigidBody* plane = (cRigidBody*)second;

		glm::vec3 c;
		c = sphere->positionOld;
		glm::vec3 velocity;
		sphere->GetVelocity(velocity);
		glm::vec3 v(sphere->position - sphere->positionOld);

		//get the shapes attached to the rigid bodies
		cSphereShape* sphereShape = dynamic_cast<cSphereShape*>(sphere->GetShape());
		cPlaneShape* planeShape = dynamic_cast<cPlaneShape*>(second->GetShape());

		float radius;
		sphereShape->GetSphereRadius(radius);
		glm::vec3 normal;
		planeShape->GetPlaneNormal(normal);

		//dot is between position and normal
		float d = glm::dot(plane->position, normal);

		float t;
		glm::vec3 q;
		//see if the plane and sphere are colliding
		int result = intersect_moving_sphere_plane(c, radius, v, normal, d, t, q);
		if (result == 1)
		{
			sphere->position = q;
			sphere->velocity = glm::reflect(sphere->velocity, normal);
			glm::vec3 nComponent = glm::proj(sphere->velocity, normal);
			sphere->velocity -= nComponent * 0.1f;
			sphere->position = (c + v * t);

			State state(sphere->position, sphere->velocity, sphere->acceleration);
			integrator.integrate(state, deltaTime*(1.0f - t));
			sphere->position = state.position;
			sphere->velocity = state.velocity;

		}
		else if (result == -1)
		{
			float dist = glm::dot(normal, sphere->position) - d;
			sphere->position += normal * (radius - dist);

			//if it is already touching the ground, counter act gravity
			if (normal == glm::vec3(0, 1, 0))
				sphere->acceleration = glm::vec3(0, 5, 0);
		}
	}
}