#pragma once
#include <iPhysicsFactory.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace nPhysics
{
	class cBulletPhysicsFactory : public iPhysicsFactory
	{
	public:
		virtual ~cBulletPhysicsFactory();

		virtual iPhysicsWorld* CreateWorld();

		virtual iRigidBody* CreateRigidBody(const sRigidBodyDesc& desc, iShape* shape);

		virtual iShape* CreateSphere(float radius);
		//virtual iShape* CreatePlane(const glm::vec3& normal, float planeConst, glm::vec3 pointOnPlane);
		virtual iShape* CreatePlane(const glm::vec3& normal, float planeConst);
	};
}