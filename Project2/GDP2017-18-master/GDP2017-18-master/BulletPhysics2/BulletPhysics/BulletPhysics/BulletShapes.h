#pragma once
#include <glm\game_math.h>
#include <iShape.h>
#include "btBulletCollisionCommon.h"
#include "btBulletDynamicsCommon.h"

namespace nPhysics
{
	class cBulletSphereShape : public iShape
	{
	public:
		cBulletSphereShape(float radius);

		virtual ~cBulletSphereShape();

		virtual bool GetSphereRadius(float& radiusOut);
		btCollisionShape* bulletShape;
	private:
		cBulletSphereShape();
		cBulletSphereShape(const cBulletSphereShape& other);
		cBulletSphereShape& operator=(const cBulletSphereShape& other);

		float radius;

	};

	class cBulletPlaneShape : public iShape
	{
	public:
		cBulletPlaneShape(const glm::vec3& normal, float planeConst, glm::vec3& pointOnPlane);
		cBulletPlaneShape(const glm::vec3& normal, float planeConst);
		virtual ~cBulletPlaneShape();

		virtual bool GetPlaneNormal(glm::vec3& normalOut);
		virtual bool GetPlaneConst(float& planeConstOut);
		btCollisionShape* bulletShape;
		glm::vec3 normal;
		float planeConst;
	private:
		cBulletPlaneShape();
		cBulletPlaneShape(const cBulletPlaneShape& other);
		cBulletPlaneShape& operator=(const cBulletPlaneShape& other);

		
	};
}