#include "cBulletPhysicsWorld.h"
#include <algorithm>
#include <iostream>
#include "BulletShapes.h"

namespace nPhysics
{
	//dtor
	cBulletPhysicsWorld::~cBulletPhysicsWorld()
	{
		for (int i = 0; i != this->rigidBodies.size(); i++)
		{
			delete this->rigidBodies[i]->shape;
			delete this->rigidBodies[i];
		}

		delete this->dynamicsWorld;
		delete this->solver; 
		delete this->overlappingPairCache;
		delete this->dispatcher;
		delete this->collisionConfiguration;
		//delete this->broadPhase;
		
	}

	cBulletPhysicsWorld::cBulletPhysicsWorld()
	{
		//broadPhase = new btDbvtBroadphase();

		//collisionConfiguration = new btDefaultCollisionConfiguration();
		//dispatcher = new btCollisionDispatcher(collisionConfiguration);

		//solver = new btSequentialImpulseConstraintSolver();

		//dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadPhase, solver, collisionConfiguration);
		//dynamicsWorld->setGravity(btVector3(0, -10.0f, 0));

		collisionConfiguration = new btDefaultCollisionConfiguration();

		///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		dispatcher = new btCollisionDispatcher(collisionConfiguration);

		///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
		overlappingPairCache = new btDbvtBroadphase();

		///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
		solver = new btSequentialImpulseConstraintSolver;

		dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

		dynamicsWorld->setGravity(btVector3(0, -10, 0));
	}

	cBulletPhysicsWorld::cBulletPhysicsWorld(btVector3 gravity)
	{
		btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();

		///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);

		///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
		btBroadphaseInterface* overlappingPairCache = new btDbvtBroadphase();

		///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
		btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;

		btDiscreteDynamicsWorld* dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

		dynamicsWorld->setGravity(gravity);
	}

	//not in hello world example
	cBulletPhysicsWorld::cBulletPhysicsWorld(glm::vec3 max, glm::vec3 min)
	{
		//this->worldMax = max;
		//this->worldMin = min;

		//btVector3 worldMinBullet(worldMin.x, worldMin.y, worldMin.z);
		//btVector3 worldMaxBullet(worldMax.x, worldMax.y, worldMax.z);
		//broadPhase = new btAxisSweep3(worldMinBullet, worldMaxBullet);

		//collisionConfiguration = new btDefaultCollisionConfiguration();
		//dispatcher = new btCollisionDispatcher(collisionConfiguration);

		//solver = new btSequentialImpulseConstraintSolver();

		//dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadPhase, solver, collisionConfiguration);
		//dynamicsWorld->setGravity(btVector3(0, -5.0f, 0));
	}


	void cBulletPhysicsWorld::TimeStep(float deltaTime)
	{

		this->dynamicsWorld->stepSimulation(deltaTime);
	}

	//add a rigid body to the scene
	void cBulletPhysicsWorld::AddRigidBody(iRigidBody* rigidBody)
	{
		//cBulletRigidBody* rb = dynamic_cast<cBulletRigidBody*>(rigidBody);
		//if (!rb)
		//{
		//	return;
		//}

		//btRigidBody* bulletBody = rb->body;
		//bulletBody->setCollisionShape(rb->collisionShape);
		//if (bulletBody->isInWorld())
		//	return;

		//bulletBody->setUserPointer(rb);

		//this->dynamicsWorld->addRigidBody(bulletBody);
		//this->collisionShapes.push_back(rb->collisionShape);
		//this->rigidBodies.push_back(rb);

		cBulletRigidBody* myBody = dynamic_cast<cBulletRigidBody*>(rigidBody);
		if (!myBody)
		{
			return; //not good :(
		}
		btRigidBody* btBody = myBody->body;
		if (btBody->isInWorld())
		{
			return; //already in the world
		}
		dynamicsWorld->addRigidBody(btBody);
		return;

	}

	//remove a rigid body from the scene
	void cBulletPhysicsWorld::RemoveRigidBody(iRigidBody* rigidBody)
	{
		//cBulletRigidBody* rb = dynamic_cast<cBulletRigidBody*>(rigidBody);
		//if (!rb)
		//{
		//	return;
		//}

		//btRigidBody* bulletBody = rb->body;
		//if (!bulletBody->isInWorld())
		//	return;
		//rb = (cBulletRigidBody*)bulletBody->getUserPointer();
		//bulletBody->setUserPointer(0);
		//this->dynamicsWorld->removeRigidBody(bulletBody);

		//std::vector<cBulletRigidBody*>::iterator itRigidBody;

		//itRigidBody = std::remove(rigidBodies.begin(), rigidBodies.end(), rb);
		//if (itRigidBody != rigidBodies.end())
		//{
		//	rigidBodies.resize(rigidBodies.size() - 1);
		//}


		cBulletRigidBody* myBody = dynamic_cast<cBulletRigidBody*>(rigidBody);
		if (!myBody)
		{
			return; //not good :(
		}
		btRigidBody* btBody = myBody->body;
		if (btBody->isInWorld())
		{
			return; //not in world already
		}
		dynamicsWorld->removeRigidBody(btBody);
		return;
	}


	void cBulletPhysicsWorld::SetDebugRenderer(iDebugRenderer * debugRenderer)
	{
	}

	void cBulletPhysicsWorld::RenderDebug()
	{
	}

	//to test for and handle a collision between 2 rigid bodies
	void cBulletPhysicsWorld::Collide(iRigidBody * first, iRigidBody * second)
	{
		eShapeType typeA = first->GetShape()->GetShapeType();
		eShapeType typeB = second->GetShape()->GetShapeType();
		switch (typeA)
		{
		case eShapeType::SHAPE_TYPE_SPHERE: //if the first shape is a sphere
		{

			cBulletSphereShape* sphereA = dynamic_cast<cBulletSphereShape*>(first->GetShape());
			switch (typeB)
			{
			case eShapeType::SHAPE_TYPE_SPHERE:
			{
				//do sphere sphere collision
				cBulletSphereShape* sphereB = dynamic_cast<cBulletSphereShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSphereSphere(first, second);
			}
			break;
			case eShapeType::SHAPE_TYPE_PLANE:
			{
				//do sphere plane collision
				cBulletPlaneShape* planeB = dynamic_cast<cBulletPlaneShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSpherePlane(first, second);
			}
			break;
			}
		}
		break;
		case eShapeType::SHAPE_TYPE_PLANE: //if the first shape is a plane
		{
			cBulletPlaneShape* planeA = dynamic_cast<cBulletPlaneShape*>(first->GetShape());
			switch (typeB)
			{
			case eShapeType::SHAPE_TYPE_SPHERE:
			{
				//do plane sphere collision
				cBulletSphereShape* sphereB = dynamic_cast<cBulletSphereShape*>(second->GetShape());

				//call a more specific method to test for and handle the collision
				return CollideSpherePlane(second, first);
			}
			break;
			}
		}
		break;
		}
	}

	void cBulletPhysicsWorld::CollideSphereSphere(iRigidBody * first, iRigidBody * second)
	{
	}

	void cBulletPhysicsWorld::CollideSpherePlane(iRigidBody * first, iRigidBody * second)
	{
	}
}