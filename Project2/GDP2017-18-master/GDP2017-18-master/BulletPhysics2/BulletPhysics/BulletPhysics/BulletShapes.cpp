#include "BulletShapes.h"

namespace nPhysics
{
	cBulletSphereShape::cBulletSphereShape(float radius)
		: iShape(SHAPE_TYPE_SPHERE)
		, radius(radius)
	{
		bulletShape = new btSphereShape(radius);
	}
	cBulletSphereShape::cBulletSphereShape()
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}

	cBulletSphereShape::cBulletSphereShape(const cBulletSphereShape& other)
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}
	cBulletSphereShape& cBulletSphereShape::operator=(const cBulletSphereShape& other)
	{
		return *this;
	}
	cBulletSphereShape::~cBulletSphereShape()
	{

	}
	bool cBulletSphereShape::GetSphereRadius(float& radiusOut)
	{
		radiusOut = radius;
		return true;
	}
	cBulletPlaneShape::cBulletPlaneShape(const glm::vec3& normal, float planeConst, glm::vec3& pointOnPlane)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{
		//bulletShape = new btStaticPlaneShape(btVector3(normal.x, normal.y, normal.z),glm::dot(normal, pointOnPlane));
	}
	cBulletPlaneShape::cBulletPlaneShape(const glm::vec3 & normal, float planeConst)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{
	}
	cBulletPlaneShape::cBulletPlaneShape()
		: iShape(SHAPE_TYPE_PLANE)
	{
	}

	cBulletPlaneShape::cBulletPlaneShape(const cBulletPlaneShape& other)
		: iShape(SHAPE_TYPE_PLANE)
	{

	}
	cBulletPlaneShape& cBulletPlaneShape::operator=(const cBulletPlaneShape& other)
	{
		return *this;
	}
	cBulletPlaneShape::~cBulletPlaneShape()
	{

	}
	bool cBulletPlaneShape::GetPlaneNormal(glm::vec3& normalOut)
	{
		normalOut = normal;
		return true;
	}
	bool cBulletPlaneShape::GetPlaneConst(float& planeConstOut)
	{
		planeConstOut = planeConst;
		return true;
	}
}