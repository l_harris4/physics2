#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <iostream>

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_movingLights;
extern bool g_boundingBoxes;
extern cCamera* g_pTheCamera;

extern nPhysics::iPhysicsFactory* gPhysicsFactory;
extern nPhysics::iPhysicsWorld* gPhysicsWorld;
extern nPhysics::iPhysicsFactory* myPhysicsFactory;
extern bool usingBullet;

const float MOVESPEED = 0.3f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;
const float PUSHSPEED = 12.0f;
//extern cGameObject* cameraTargetObject;



void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->adjustQOrientationFormDeltaEuler(turnChange);
	//glm::vec3 targetPosition = g_pTheCamera->theObject->position;

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);

	//g_pTheCamera->target = targetPosition;

	glm::vec3 objectPos;
	g_pTheCamera->theObject->rBody->GetPosition(objectPos);
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}

void updateCamera()
{
	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);

	//g_pTheCamera->target = targetPosition;

	glm::vec3 objectPos;
	g_pTheCamera->theObject->rBody->GetPosition(objectPos);
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}


/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	switch (key)
	{
	case GLFW_KEY_SPACE:
		if (action == GLFW_PRESS)
		{
			::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[0].blendRatio = 1.0f;
			::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[1].blendRatio = 0.0f;
			::g_selectedGameObjectIndex++;
			if (::g_selectedGameObjectIndex >= 10)
			{
				::g_selectedGameObjectIndex = 1;
			}

			::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[0].blendRatio = 0.0f;
			::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[1].blendRatio = 1.0f;
		}
		break;
	case GLFW_KEY_Z:
		if (action == GLFW_PRESS)
		{
			g_texturesOn = !g_texturesOn;
		}
		break;
	case GLFW_KEY_U://cycle through objects
		if (action == GLFW_PRESS)
		{
			::g_selectedGameObjectIndex++;
			if (::g_selectedGameObjectIndex >= ::g_vecGameObjects.size())
				::g_selectedGameObjectIndex = 0;
			::g_selectedLightIndex++;
			if (::g_selectedLightIndex >= ::g_pLightManager->vecLights.size())
				::g_selectedLightIndex = 0;
		}
		break;
	case GLFW_KEY_I://Toggle between controlling light/object/camera
		if (action == GLFW_PRESS)
		{
			if (::g_movingGameObject)
			{
				::g_movingGameObject = false;
				::g_movingLights = true;
			}
			else if (::g_movingLights)
			{
				::g_movingLights = false;
			}
			else
			{
				::g_movingGameObject = true;
			}
		}
		break;
	case GLFW_KEY_A:// Move an object or light negative in x axis, or rotate camera negative in x axis
		if (::g_movingGameObject)
		{
			glm::vec3 tempPos;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

			glm::vec3 pushValue = glm::normalize(glm::cross((g_pTheCamera->eye - tempPos), glm::vec3(0, 1, 0))) * PUSHSPEED;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(pushValue);
		}
		break;
	case GLFW_KEY_D:// Move an object or light positive in x axis, or rotate camera positive in x axis
		if (::g_movingGameObject)
		{
			glm::vec3 tempPos;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

			glm::vec3 pushValue = glm::normalize(glm::cross((g_pTheCamera->eye - tempPos), glm::vec3(0,1,0)) ) * PUSHSPEED;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(-pushValue);
		}
		break;
	case GLFW_KEY_W:// Move an object or light negative in z axis, or rotate camera negative in z axis
		if (::g_movingGameObject)
		{		
			glm::vec3 tempPos;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

			glm::vec3 pushValue = glm::normalize(g_pTheCamera->eye - tempPos) * PUSHSPEED;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(-pushValue);
		}
		break;
	case GLFW_KEY_S:// Move an object or light positive in z axis, or rotate camera positive in z axis
		if (::g_movingGameObject)
		{
			glm::vec3 tempPos;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

			glm::vec3 pushValue = glm::normalize(g_pTheCamera->eye - tempPos) * PUSHSPEED;
			::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(pushValue);
		}
		break;
	case GLFW_KEY_Q://Switch physics worlds
		if (action == GLFW_PRESS)
		{
			usingBullet = !usingBullet;
			//now using the old world create the new one

			nPhysics::iPhysicsFactory* currentFactory;

			if (usingBullet)
				currentFactory = gPhysicsFactory;
			else
				currentFactory = myPhysicsFactory;

			//TODO make a set velocity function to transfer the velocities

			nPhysics::iPhysicsWorld* tempPhysicsWorld;
			tempPhysicsWorld = currentFactory->CreateWorld();

			for (int i = 0; i < ::g_vecGameObjects.size(); ++i)
			{
				if (::g_vecGameObjects[i]->rBody != 0 && ::g_vecGameObjects[i]->bIsUpdatedInPhysics)
				{
					nPhysics::sRigidBodyDesc desc;
					glm::vec3 tempPos;
					if (::g_vecGameObjects[i]->bIsUpdatedInPhysics)
					{
						::g_vecGameObjects[i]->rBody->GetPosition(tempPos);
					}
					else
					{
						tempPos = ::g_vecGameObjects[i]->position;
					}

					desc.Position = tempPos;
					nPhysics::iShape* shape;
					glm::vec3 tempVelocity = glm::vec3(0);
					if (::g_vecGameObjects[i]->typeOfObject == eTypeOfObject::SPHERE)
					{
						shape = currentFactory->CreateSphere(::g_vecGameObjects[i]->scale);
						::g_vecGameObjects[i]->rBody->GetVelocity(tempVelocity);
					}
					else if (::g_vecGameObjects[i]->typeOfObject == eTypeOfObject::PLANE)
					{
						glm::vec3 norm;
						::g_vecGameObjects[i]->rBody->GetShape()->GetPlaneNormal(norm);
						shape = currentFactory->CreatePlane(norm, 1);
					}


					//gPhysicsFactory->CreateRigidBody(desc, shape);
					::g_vecGameObjects[i]->rBody = currentFactory->CreateRigidBody(desc, shape);
					if (::g_vecGameObjects[i]->typeOfObject == eTypeOfObject::PLANE)
					{
						::g_vecGameObjects[i]->rBody->SetStatic(true);
					}
					else
					{
						::g_vecGameObjects[i]->rBody->SetVelocity(tempVelocity);
					}


					tempPhysicsWorld->AddRigidBody(::g_vecGameObjects[i]->rBody);
				}

			}
			//To build a better world, sometimes means tearing the old one down
			delete gPhysicsWorld;
			gPhysicsWorld = tempPhysicsWorld;



		}
		break;
	case GLFW_KEY_E:// Move an object or light positive in y axis, or rotate camera positive in y axis
		if (::g_movingGameObject)
		{
			//::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y += MOVESPEED;
		}
		else if (::g_movingLights)
		{
			//::g_pLightManager->vecLights[g_selectedLightIndex].position.y += MOVESPEED;
			//if (::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex != -1)
			//{
			//	int localIndex = ::g_pLightManager->vecLights[g_selectedLightIndex].gameObjectIndex;
			//	::g_vecGameObjects[localIndex]->position.y += MOVESPEED;
			//}
		}
		else
		{
			//glm::vec4 tempPos = glm::toMat4(g_pTheCamera->theObject->qOrientation) *
			//	glm::vec4(0, 0, -CAMERASPEED, 1);

			//glm::vec3 position = glm::vec3(tempPos.x + g_pTheCamera->theObject->position.x
			//	, tempPos.y + g_pTheCamera->theObject->position.y,
			//	tempPos.z + g_pTheCamera->theObject->position.z);

			//g_pTheCamera->theObject->position = position;
			//updateCamera();
		}
		break;
	case GLFW_KEY_P: //used for debugging, printing the current location of object
		if (::g_movingGameObject)
		{
			//std::cout << "x:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.x
			//	<< " y:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.y
			//	<< " z:" << ::g_vecGameObjects[::g_selectedGameObjectIndex]->position.z
			//	<< std::endl;
		}
		else if (::g_movingLights)
		{
			std::cout << "x:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.x
				<< " y:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.y
				<< " z:" << ::g_pLightManager->vecLights[::g_selectedLightIndex].position.z
				<< std::endl;
		}
		break;

	case GLFW_KEY_O: //Switching an object between wireframe or not
		if (action == GLFW_PRESS) {
			g_boundingBoxes = !g_boundingBoxes;
		}
		break;
	case GLFW_KEY_L: //Switching an object between wireframe or not
		if (action == GLFW_PRESS) {
			//SaveDataToFile();
		}
		break;
	case GLFW_KEY_C: //duplicating an object
		if (action == GLFW_PRESS) {
			cGameObject* pTempGO = new cGameObject();
			pTempGO->bIsUpdatedInPhysics = ::g_vecGameObjects[::g_selectedGameObjectIndex]->bIsUpdatedInPhysics;
			pTempGO->bIsWireFrame = ::g_vecGameObjects[::g_selectedGameObjectIndex]->bIsWireFrame;
			pTempGO->diffuseColour = ::g_vecGameObjects[::g_selectedGameObjectIndex]->diffuseColour;
			pTempGO->meshName = ::g_vecGameObjects[::g_selectedGameObjectIndex]->meshName;
			pTempGO->qOrientation = ::g_vecGameObjects[::g_selectedGameObjectIndex]->qOrientation;

			nPhysics::sRigidBodyDesc desc;
			glm::vec3 position;
			::g_vecGameObjects[::g_selectedGameObjectIndex]->rBody->GetPosition(position);
			desc.Position = position;
			nPhysics::iShape* shape = gPhysicsFactory->CreateSphere(1);


			//gPhysicsFactory->CreateRigidBody(desc, shape);
			pTempGO->rBody = gPhysicsFactory->CreateRigidBody(desc, shape);

			pTempGO->radius = ::g_vecGameObjects[::g_selectedGameObjectIndex]->radius;
			pTempGO->scale = ::g_vecGameObjects[::g_selectedGameObjectIndex]->scale;
			pTempGO->typeOfObject = ::g_vecGameObjects[::g_selectedGameObjectIndex]->typeOfObject;
			pTempGO->vel = ::g_vecGameObjects[::g_selectedGameObjectIndex]->vel;

			::g_vecGameObjects.push_back(pTempGO);

		}
		break;

		//1 - 6 are used to change the rotation of an object
	case GLFW_KEY_1:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(glm::radians(1.0f),0,0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.x += 0.01;
		}

		break;
	case GLFW_KEY_2:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(glm::radians(-1.0f), 0, 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.x -= 0.01;
		}
		break;
	case GLFW_KEY_3:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, glm::radians(1.0f), 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.y += 0.01;
		}
		break;
	case GLFW_KEY_4:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, glm::radians(-1.0f), 0));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.y -= 0.01;
		}
		break;
	case GLFW_KEY_5:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, 0, glm::radians(1.0f)));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.z += 0.01;
		}
		break;
	case GLFW_KEY_6:
		if (::g_movingGameObject)
		{
			::g_vecGameObjects[::g_selectedGameObjectIndex]->adjustQOrientationFormDeltaEuler(glm::vec3(0, 0, glm::radians(-1.0f)));
		}
		else if (::g_movingLights)
		{
			::g_pLightManager->vecLights[g_selectedLightIndex].attenuation.z -= 0.01;
		}
		break;

	case GLFW_KEY_UP: //Move the camera "forward"
	{
		g_pTheCamera->zDist += 0.5;
		updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_DOWN: //Move the camera "backward"
	{
		g_pTheCamera->zDist -= 0.5;
		updateCamera(glm::vec3(0, 0, 0));
	}
	break;
	case GLFW_KEY_LEFT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_RIGHT: //Move the camera "backward"
	{
		updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
	}
	break;


	}

	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods && GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

//bool isCtrlKeyDown( int mods, bool bByItself = true );
//bool isAltKeyDown( int mods, bool bByItself = true );