--------------------------------------------
CONTROLS
--------------------------------------------
Q: to switch which physics library is being used
	the rotation in mine was always too fast and did't work correctly. That would be an easy way to tell which 
	library is currently being used
O: to turn debug mode on and off
 -in debug mode you should see bounding boxes of the spheres and the spheres should turn white at moment of collision
 
WASD: are the controls to move the controlled ball
Space: to change which ball is being controlled
Left and right arrow keys: to rotate around the controlled ball
Up and down arrow keys: to zoom in and out