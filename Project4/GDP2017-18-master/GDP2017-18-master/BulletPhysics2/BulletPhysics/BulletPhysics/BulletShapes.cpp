#include "BulletShapes.h"

namespace nPhysics
{
	//simple conversion method
	inline btVector3 ToBullet(const glm::vec3& inVec) {
		return btVector3(inVec.x, inVec.y, inVec.z);
	}

	//ctor
	cBulletSphereShape::cBulletSphereShape(float radius)
		: iShape(SHAPE_TYPE_SPHERE)
		, radius(radius)
	{
		bulletShape = new btSphereShape(radius);
	}
	//ctor
	cBulletSphereShape::cBulletSphereShape()
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}
	//ctor
	cBulletSphereShape::cBulletSphereShape(const cBulletSphereShape& other)
		: iShape(SHAPE_TYPE_SPHERE)
	{

	}
	//ctor
	cBulletSphereShape& cBulletSphereShape::operator=(const cBulletSphereShape& other)
	{
		return *this;
	}
	//dtor
	cBulletSphereShape::~cBulletSphereShape()
	{

	}
	//getter for a sphere radius
	bool cBulletSphereShape::GetSphereRadius(float& radiusOut)
	{
		radiusOut = radius;
		return true;
	}
	//ctor
	cBulletPlaneShape::cBulletPlaneShape(const glm::vec3& normal, float planeConst, glm::vec3& pointOnPlane)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{
		
	}
	//ctor
	cBulletPlaneShape::cBulletPlaneShape(const glm::vec3 & normal, float planeConst)
		: iShape(SHAPE_TYPE_PLANE)
		, normal(normal)
		, planeConst(planeConst)
	{
	}
	//ctor
	cBulletPlaneShape::cBulletPlaneShape()
		: iShape(SHAPE_TYPE_PLANE)
	{
	}
	//ctor
	cBulletPlaneShape::cBulletPlaneShape(const cBulletPlaneShape& other)
		: iShape(SHAPE_TYPE_PLANE)
	{

	}
	//ctor
	cBulletPlaneShape& cBulletPlaneShape::operator=(const cBulletPlaneShape& other)
	{
		return *this;
	}
	//dtor
	cBulletPlaneShape::~cBulletPlaneShape()
	{

	}
	//getter for a plane normal
	bool cBulletPlaneShape::GetPlaneNormal(glm::vec3& normalOut)
	{
		normalOut = normal;
		return true;
	}
	//getter for a plane const
	bool cBulletPlaneShape::GetPlaneConst(float& planeConstOut)
	{
		planeConstOut = planeConst;
		return true;
	}
	//ctor
	cBulletBoxShape::cBulletBoxShape(glm::vec3 boxHalfWidths) : iShape(SHAPE_TYPE_BOX)
	{
		bulletShape = new btBoxShape(ToBullet(boxHalfWidths));
	}
	//dtor
	cBulletBoxShape::~cBulletBoxShape()
	{
	}
	//ctor
	cBulletCylinderShape::cBulletCylinderShape(glm::vec3 boxHalfWidths)
	{
		//the commented out ones are options for aligning the cylinder on a different axes
		//bulletShape = new btCylinderShape(ToBullet(boxHalfWidths));
		bulletShape = new btCylinderShapeX(ToBullet(boxHalfWidths));
		//bulletShape = new btCylinderShapeZ(ToBullet(boxHalfWidths));
	}
	//dtor
	cBulletCylinderShape::~cBulletCylinderShape()
	{
	}
	//ctor
	cBulletConeShape::cBulletConeShape(float height, float radius)
	{
		bulletShape = new btConeShape(height, radius);
	}
	//dtor
	cBulletConeShape::~cBulletConeShape()
	{
	}
}