#include "cBulletRigidBody.h"
#include "BulletShapes.h"
#include "nConvert.h"

namespace nPhysics
{
	//ctor
	cBulletRigidBody::cBulletRigidBody(const sRigidBodyDesc& desc, iShape* shape)
		: shape(shape)
		, position(desc.Position)
		, velocity(desc.Velocity)
		, mass(desc.Mass)
		, rotation(desc.Rotation)
		, isStatic(false)
		, acceleration(glm::vec3(0,0,0))
		, orientationVelocity(glm::quat())
		, colliding(false)
		, collidingCounter(0.0f)
		
	{

		orientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));
		btCollisionShape* colShape = 0;
		//if it is a plane make it static
		if (shape->GetShapeType() == SHAPE_TYPE_PLANE)
		{
			isStatic = true;

			glm::vec3 planeConst = desc.Position + ((cBulletPlaneShape*)shape)->normal * 30.0f;

			((cBulletPlaneShape*)shape)->bulletShape = 
				new btStaticPlaneShape(nConvert::ToBullet(((cBulletPlaneShape*)shape)->normal), 0);

			colShape = ((cBulletPlaneShape*)shape)->bulletShape;
		}
		else //otherwise just set the shape
		{
			colShape = ((cBulletSphereShape*)shape)->bulletShape;
		}

		// Create Dynamic Objects
		btTransform startTransform;
		startTransform.setOrigin(nConvert::ToBullet(desc.Position));
		startTransform.setIdentity();

		btScalar mass(desc.Mass);

		//two ways to tell if an object is static
		if (mass == 0.f || isStatic)
		{
			mass = 0.0f;
			isStatic = true;
		}

		btVector3 localInertia(0, 0, 0);
		if (!isStatic)
			colShape->calculateLocalInertia(mass, localInertia);

		startTransform.setOrigin(btVector3(desc.Position.x, desc.Position.y, desc.Position.z));

		//using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
		motionState = new btDefaultMotionState(startTransform);
		btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, motionState, colShape, localInertia);
		body = new btRigidBody(rbInfo);

		body->setUserPointer(this);

	}
	//dtor
	cBulletRigidBody::~cBulletRigidBody()
	{
		body->setUserPointer(0);
		delete body;
		delete motionState;
		delete shape;
		shape = 0;
	}

	//getters and setters
	iShape* cBulletRigidBody::GetShape()
	{
		return shape;
	}

	void cBulletRigidBody::GetPosition(glm::vec3& positionOut)
	{
		nConvert::ToGlm(body->getCenterOfMassPosition(), positionOut);
	}
	void cBulletRigidBody::GetRotation(glm::vec3& rotationOut)
	{
		rotationOut = glm::eulerAngles(rotation);
	}
	void cBulletRigidBody::SetStatic(bool isStat)
	{
		isStatic = isStat;
	}
	void cBulletRigidBody::GetVelocity(glm::vec3 & velocityOut)
	{
		velocityOut = nConvert::ToGlm(this->body->getLinearVelocity());
	}
	void cBulletRigidBody::SetVelocity(glm::vec3 & velocityIn)
	{
		this->body->setLinearVelocity(nConvert::ToBullet(velocityIn));
	}
	void cBulletRigidBody::Push(glm::vec3 accel)
	{
		body->activate(true);
		body->applyCentralForce(nConvert::ToBullet(accel));
	}
	void cBulletRigidBody::GetOrientation(glm::quat & orientation)
	{
		orientation = nConvert::ToGlm(this->body->getOrientation());
	}
	void cBulletRigidBody::GetColliding(bool & colliding)
	{
		colliding = this->colliding;
	}

	void cBulletRigidBody::SetColliding(bool colliding)
	{
		if (colliding == true)
		{
			collidingCounter = 4.0f;
			this->colliding = true;
		}
		else
		{
			collidingCounter -= 0.1f;
			if (collidingCounter < 0)
			{
				this->colliding = false;
				collidingCounter = 0.0f;
			}
		}
		
	}

	void cBulletRigidBody::GetTransform(glm::mat4& transformOut)
	{
		nConvert::ToGlm(body->getWorldTransform(), transformOut);
	}
}