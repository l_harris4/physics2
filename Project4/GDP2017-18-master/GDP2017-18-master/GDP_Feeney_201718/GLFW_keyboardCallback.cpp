#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"
#include "Input.h"

#include <iostream>

bool isShiftKeyDown( int mods, bool bByItself = true );
bool isCtrlKeyDown( int mods, bool bByItself = true );
bool isAltKeyDown( int mods, bool bByItself = true );

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_movingLights;
extern bool g_boundingBoxes;
extern cCamera* g_pTheCamera;

extern nPhysics::iPhysicsFactory* gPhysicsFactory;
extern nPhysics::iPhysicsWorld* gPhysicsWorld;
extern bool usingBullet;

const float MOVESPEED = 0.3f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;
const float PUSHSPEED = 12.0f;
bool spacePressed = false;



void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->adjustQOrientationFormDeltaEuler(turnChange);
	//glm::vec3 targetPosition = g_pTheCamera->theObject->position;

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);

	//g_pTheCamera->target = targetPosition;

	glm::vec3 objectPos;
	g_pTheCamera->theObject->rBody->GetPosition(objectPos);
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}

void updateCamera()
{
	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);

	//g_pTheCamera->target = targetPosition;

	glm::vec3 objectPos;
	g_pTheCamera->theObject->rBody->GetPosition(objectPos);
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}

//the new input method
void checkInput()
{
	if (Input::IsKeyPressed::Space() == true && spacePressed == false)
	{
		spacePressed = true;
		::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[0].blendRatio = 1.0f;
		::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[1].blendRatio = 0.0f;
		::g_selectedGameObjectIndex++;
		if (::g_selectedGameObjectIndex >= 14)
		{
			::g_selectedGameObjectIndex = 1;
		}

		::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[0].blendRatio = 0.0f;
		::g_vecGameObjects[g_selectedGameObjectIndex]->vecMeshes[0].vecMehs2DTextures[1].blendRatio = 1.0f;
	}

	if (Input::IsKeyPressed::Space() == false)
	{
		spacePressed = false;
	}

	if (Input::IsKeyPressed::A() == true)
	{
		glm::vec3 tempPos;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

		glm::vec3 pushValue = glm::normalize(glm::cross((g_pTheCamera->eye - tempPos), glm::vec3(0, 1, 0))) * PUSHSPEED;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(pushValue);
	}

	if (Input::IsKeyPressed::D() == true)
	{
		glm::vec3 tempPos;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

		glm::vec3 pushValue = glm::normalize(glm::cross((g_pTheCamera->eye - tempPos), glm::vec3(0, 1, 0))) * PUSHSPEED;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(-pushValue);
	}

	if (Input::IsKeyPressed::W() == true)
	{
		glm::vec3 tempPos;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

		glm::vec3 pushValue = glm::normalize(g_pTheCamera->eye - tempPos) * PUSHSPEED;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(-pushValue);
	}

	if (Input::IsKeyPressed::S() == true)
	{
		glm::vec3 tempPos;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->GetPosition(tempPos);

		glm::vec3 pushValue = glm::normalize(g_pTheCamera->eye - tempPos) * PUSHSPEED;
		::g_vecGameObjects[g_selectedGameObjectIndex]->rBody->Push(pushValue);
	}

	if (Input::IsKeyPressed::Left() == true)
	{
		updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
	}

	if (Input::IsKeyPressed::Right() == true)
	{
		updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
	}

	if (Input::IsKeyPressed::Up() == true)
	{
		g_pTheCamera->zDist += 0.5;
		updateCamera(glm::vec3(0, 0, 0));
	}

	if (Input::IsKeyPressed::Down() == true)
	{
		g_pTheCamera->zDist -= 0.5;
		updateCamera(glm::vec3(0, 0, 0));
	}
}


//this key_callback is no longer used as we would get zero on the project
/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);

	switch (key)
	{


	case GLFW_KEY_O: //Switching an object between wireframe or not
		if (action == GLFW_PRESS) {
			g_boundingBoxes = !g_boundingBoxes;
		}
		break;


	}

	return;
}



// Helper functions
bool isShiftKeyDown( int mods, bool bByItself /*=true*/ )
{
	if ( bByItself )
	{	// shift by itself?
		if ( mods == GLFW_MOD_SHIFT )	{ return true; }
		else							{ return false; }
	}
	else
	{	// shift with anything else, too
		if ( ( mods && GLFW_MOD_SHIFT ) == GLFW_MOD_SHIFT )	{	return true;	}
		else												{	return false;	}
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}
